# How to use:

Put a file named .brige in the localhost git folder repo.
Example:

```
[default]
ignore_submodules: path/submodule/a path/submodule/b

[target1]
server: login.target1.company.com
target: /remote/path/mygit

[target2]
server: login.target2.company.com
target: /remote/path/mygit
```

In order to use it, you just need to run this in localhost shell to sync with the remote:

```
brige sync target1 -f
```
